# -*- coding: utf-8 -*-


class Solution:
    def lengthOfLongestSubstring(self, s: str) -> int:

        child = []
        length = 0
        for c in s:
            if c in child:
                length = max(length, len(child))
                child[:] = child[child.index(c) + 1:]
            child.append(c)
        return max(length, len(child))
