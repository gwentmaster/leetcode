# -*- coding: utf-8 -*-


from typing import List


class Solution:
    def generateParenthesis(self, n: int) -> List[str]:

        result = set()

        def recursion(left_left, right_left, summary, string):

            if left_left > 0 and right_left == 0:
                return None
            elif left_left > 0 and right_left > 0:
                if summary == 0:
                    result.add(recursion(
                        left_left-1,
                        right_left,
                        summary+1,
                        string+"("
                    ))
                else:
                    result.add(recursion(
                        left_left-1,
                        right_left,
                        summary+1,
                        string+"(")
                    )
                    result.add(recursion(
                        left_left,
                        right_left-1,
                        summary-1,
                        string+")")
                    )
            else:
                return string + ")"*right_left

        recursion(n, n, 0, "")
        result.discard(None)
        return list(result)


hh = Solution().generateParenthesis(3)
print(hh)
