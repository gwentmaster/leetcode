# -*- coding: utf-8 -*-


class Solution:
    def isPalindrome(self, s: str) -> bool:
        li = [
            c.upper()
            for c in s
            if (
                ord("a") <= ord(c) <= ord("z")
                or ord("A") <= ord(c) <= ord("Z")
                or ord("0") <= ord(c) <= ord("9")
            )
        ]
        return li == li[::-1]


hh = Solution().isPalindrome("hellolleh")
print(hh)
