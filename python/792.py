# -*- coding: utf-8 -*-


from typing import Dict, List


class Solution:
    def numMatchingSubseq(self, S: str, words: List[str]) -> int:
        result = 0
        string_dic = {}  # type: Dict[str, int]
        for w in words:
            string_dic[w] = string_dic.setdefault(w, 0) + 1
        for w, count in string_dic.items():
            bound = 0
            for char in w:
                try:
                    bound = S.index(char, bound) + 1
                except ValueError:
                    break
            else:
                result += count
        return result
