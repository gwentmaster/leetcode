# -*- coding: utf-8 -*-


class Solution:
    def hammingWeight(self, n: int) -> int:
        return [i for i in f"{n:b}"].count("1")
