# -*- coding: utf-8 -*-


"""
1260. 二维网格迁移

https://leetcode-cn.com/problems/shift-2d-grid/
"""


from typing import List


class Solution:

    def shiftGrid(self, grid: List[List[int]], k: int) -> List[List[int]]:

        line_num = len(grid)
        col_num = len(grid[0])

        group, h_step = divmod(k, col_num)

        transposed = [
            [grid[i][j] for i in range(line_num)]
            for j in range(col_num)
        ]

        h_zero_index = col_num - h_step
        transposed[:] = transposed[h_zero_index:] + transposed[:h_zero_index]

        v_zero_index = line_num - (group + 1) % line_num
        for i in range(h_step):
            transposed[i][:] = (
                transposed[i][v_zero_index:] + transposed[i][:v_zero_index]
            )

        v_zero_index = line_num - group % line_num
        for i in range(h_step, col_num):
            transposed[i][:] = (
                transposed[i][v_zero_index:] + transposed[i][:v_zero_index]
            )

        return [
            [transposed[j][i] for j in range(col_num)]
            for i in range(line_num)
        ]


hh = Solution().shiftGrid(
    [[1, 2, 3], [4, 5, 6], [7, 8, 9]],
    1
)
print(hh)
