struct Solution();
impl Solution {
    pub fn hammingWeight(n: u32) -> i32 {
        let mut count: u32 = 0;
        let mut nn = n;
        while nn != 0 {
            count += (nn % 2);
            nn = nn / 2;
        }
        count as i32
    }
}
fn main() {
    let hh = Solution::hammingWeight(12);
    println!("{}", hh);
}
