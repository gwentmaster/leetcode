struct Solution();
impl Solution {
    pub fn length_of_longest_substring(s: String) -> i32 {
        let mut child: Vec<char> = vec![];
        let mut length: usize = 0;
        let mut index: usize = 0;
        let mut in_flag: bool;
        for character in s.chars() {
            in_flag = false;
            for i in 0..child.len() {
                if child[i] == character {
                    index = i;
                    in_flag = true;
                    break;
                }
            }
            length = if length > child.len() {
                length
            } else {
                child.len()
            };
            if in_flag {
                for _ in 0..index + 1 {
                    child.remove(0);
                }
            }
            child.push(character);
        }
        if length > child.len() {
            length as i32
        } else {
            child.len() as i32
        }
    }
}
fn main() {
    let hh = Solution::length_of_longest_substring("pwwkew".to_string());
    println!("{}", hh);
}
