struct Solution();
impl Solution {
    pub fn merge(nums1: &mut Vec<i32>, m: i32, nums2: &mut Vec<i32>, n: i32) {
        if m == 0 {
            for i in 0..nums1.len() {
                nums1[i] = nums2[i];
            }
            return;
        }
        let mut temp: Vec<i32> = vec![];
        let mut i: usize = 0;
        let mut j: usize = 0;
        while i < m as usize || j < n as usize {
            if i >= m as usize {
                temp.push(nums2[j]);
                i += 1;
                j += 1;
                continue;
            }
            if j >= n as usize {
                temp.push(nums1[i]);
                i += 1;
                j += 1;
                continue;
            }
            if nums1[i] <= nums2[j] {
                temp.push(nums1[i]);
                i += 1;
            } else {
                temp.push(nums2[j]);
                j += 1;
            }
        }
        for index in 0..temp.len() {
            nums1[index] = temp[index]
        }
    }
}
fn main() {
    let mut mm = vec![0, 0, 0, 0, 0];
    let mut nn = vec![1, 2, 3, 4, 5];
    let hh = Solution::merge(&mut mm, 0, &mut nn, 5);
    println!("{:?}", mm);
    // println!("{}", hh);
}
