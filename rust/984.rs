struct Solution();
impl Solution {
    pub fn str_without3a3b(a: i32, b: i32) -> String {
        let mut result = "".to_string();
        let bigger: i32;
        let smaller: i32;
        let bigger_char: char;
        let smaller_char: char;
        if a > b {
            bigger = a;
            smaller = b;
            bigger_char = 'a';
            smaller_char = 'b';
        } else {
            bigger = b;
            smaller = a;
            bigger_char = 'b';
            smaller_char = 'a';
        }
        let single_bound: i32;
        if bigger > 2 * smaller {
            single_bound = if bigger % 2 == 0 { 0 } else { 1 };
        } else {
            single_bound = 2 * smaller - bigger;
        }
        let mut get_small = false;
        for i in 0..2 * smaller {
            if get_small {
                result.push(smaller_char);
            } else {
                result.push(bigger_char);
                if i >= 2 * single_bound {
                    result.push(bigger_char);
                }
            }
            get_small = !get_small;
        }
        if bigger > 2 * smaller {
            result.push(bigger_char);
            result.push(bigger_char);
        }
        result
    }
}
fn main() {
    let hh = Solution::str_without3a3b(1, 3);
    println!("{:?}", hh);
}
