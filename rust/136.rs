struct Solution {} // 改自题解
impl Solution {
    pub fn single_number(nums: Vec<i32>) -> i32 {
        let mut result = 0;
        for num in nums.into_iter() {
            result ^= num;
        }
        result
    }
}
fn main() {
    let hh = Solution::single_number(vec![1, 2, 2, 3, 3]);
    println!("{:?}", hh);
}
