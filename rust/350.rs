struct Solution {}
impl Solution {
    pub fn intersect(nums1: Vec<i32>, nums2: Vec<i32>) -> Vec<i32> {
        use std::collections::HashMap;
        let mut result: Vec<i32> = vec![];
        let mut counter: HashMap<i32, u32> = HashMap::new();
        for i in nums1 {
            *counter.entry(i).or_insert(0) += 1;
        }
        for i in nums2 {
            if let Some(count) = counter.get_mut(&i) {
                if *count == 0 {
                    continue;
                }
                result.push(i);
                *count -= 1;
            }
        }
        result
    }
}
fn main() {
    let hh = Solution::intersect(vec![4, 9, 5], vec![9, 4, 9, 8, 4]);
    println!("{:?}", hh);
}
