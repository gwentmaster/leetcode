struct Solution();
impl Solution {
    pub fn remove_duplicates(nums: &mut Vec<i32>) -> i32 {
        if nums.len() < 2 {
            return nums.len() as i32;
        }
        let mut i = 1;
        while i < nums.len() {
            if nums[i] == nums[i - 1] {
                nums.remove(i);
            } else {
                i += 1
            }
        }
        nums.len() as i32
    }
}
fn main() {
    let hh = Solution::remove_duplicates(&mut vec![1, 2, 3]);
    println!("{}", hh);
}
