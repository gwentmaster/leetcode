struct Solution();
impl Solution {
    pub fn is_palindrome(s: String) -> bool {
        let mut chars = vec![];
        for c in s.chars() {
            if (c >= '0' && c <= '9') || (c >= 'a' && c <= 'z') {
                chars.push(c);
            } else if c >= 'A' && c <= 'Z' {
                chars.push((c as u8 + 32) as char)
            }
        }
        let length = chars.len();
        for i in 0..length / 2 {
            if chars[i] != chars[length - i - 1] {
                return false;
            }
        }
        true
    }
}
fn main() {
    let hh = Solution::is_palindrome("hh".to_string());
    println!("{}", hh);
}
