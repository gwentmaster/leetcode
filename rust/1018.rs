// 优化: 不用等到 value % 5 == 0 时再令value = 0
// 而是直接在每次迭代令value = (value * 2 + num) % 5
struct Solution();
impl Solution {
    pub fn prefixes_div_by5(a: Vec<i32>) -> Vec<bool> {
        let mut result: Vec<bool> = vec![];
        let mut value = 0u64;
        for num in a.into_iter() {
            value = value * 2 + num as u64;
            if value % 5 == 0 {
                result.push(true);
                value = 0;
            } else {
                result.push(false);
            }
        }
        result
    }
}
fn main() {
    let hh = Solution::prefixes_div_by5(vec![
        1, 0, 0, 1, 0, 1, 0, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0,
        0, 1, 1, 0, 1, 0, 0, 0, 1,
    ]);
    println!("{:?}", hh);
}
