struct Solution();
impl Solution {
    pub fn is_power_of_three(n: i32) -> bool {
        let mut num = n;
        while num > 1 {
            if num % 3 != 0 {
                return false;
            }
            num = num / 3;
        }
        if num == 1 {
            true
        } else {
            false
        }
    }
}
fn main() {
    let hh = Solution::is_power_of_three(12);
    println!("{:?}", hh);
}
