struct Solution();
impl Solution {
    pub fn remove_element(nums: &mut Vec<i32>, val: i32) -> i32 {
        let length = nums.len();
        if length == 0 {
            return 0;
        }
        let mut i = 0;
        let mut j = 0;
        while j < length {
            if nums[j] != val {
                nums[i] = nums[j];
                i += 1
            }
            j += 1
        }

        i as i32
    }
}
fn main() {
    let hh = Solution::remove_element(&mut vec![1, 2, 3], 3);
    println!("{}", hh);
}
