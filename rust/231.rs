struct Solution {}
impl Solution {
    pub fn is_power_of_two(n: i32) -> bool {
        if n <= 0 {
            return false;
        }
        if n == 1 {
            return true;
        }
        let mut value = n;
        while value > 2 {
            if value % 2 != 0 {
                return false;
            }
            value = value / 2;
        }
        true
    }
}
fn main() {
    let hh = Solution::is_power_of_two(16);
    println!("{:?}", hh);
}
