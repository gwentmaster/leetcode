struct Solution();
impl Solution {
    pub fn find_the_difference(s: String, t: String) -> char {
        use std::collections::HashMap;
        let mut ss: HashMap<char, i32> = HashMap::new();
        for c in s.chars() {
            match ss.get(&c) {
                Some(i) => {
                    ss.insert(c, i + 1);
                }
                None => {
                    ss.insert(c, 1);
                }
            }
        }
        for c in t.chars() {
            match ss.get(&c) {
                Some(i) => {
                    if i < &1 {
                        return c;
                    } else {
                        ss.insert(c, i - 1);
                    }
                }
                None => {
                    return c;
                }
            }
        }
        panic!();
    }
}
fn main() {
    let hh = Solution::find_the_difference("hh".to_string(), "hhi".to_string());
    println!("{:?}", hh);
}
